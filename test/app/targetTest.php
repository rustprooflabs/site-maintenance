<?php
namespace SiteMaintenance;
require_once(dirname(__FILE__) . '/../../app/target.php');
require_once(dirname(__FILE__) . '/../../SiteMaintenanceConfig.php');

class targetTest Extends \PHPUnit_Extensions_Database_TestCase
{	
	private $config;
	
	public function __construct()
	{
		$this->config = new SiteMaintenanceConfig();
	}
	
	public function getConnection()
    {
		$config = $this->config;
		$dbname = $config->dbName;
		$username = $config->dbUser;
		$password = $config->dbPassword;
		
        $pdo = new \PDO("mysql: host=localhost; dbname=$dbname", $username, $password);
        return $this->createDefaultDBConnection($pdo, $dbname);
    }
	
	public function getDataSet()
	{
		return $this->createXMLDataSet(dirname(__FILE__).'/../_files/target_one_record.xml');
	}
	
	public function testGetTargets_Returns_Array_with_target()
	{
		$t = new Target();
		$targets = $t->getTargets();
		
		$expected = array(
			array("id" => "1", "name" => "www_test2", 
				"displayName" => "Test 2 Domain (/var/www/test2)", 
				"targetDesc" => "NONE"
				)
			);
		
		$this->assertEquals($expected, $targets);
	}
	
	public function testGetTargetPath_Returns_filepath_to_Target()
	{
		$t = new Target();
		$t->setTarget(1);
		$targetPath = $t->getTargetPath();
		
		$this->assertEquals('/var/www/test2', $targetPath);
	}
	
	
	public function testSetTarget_ReturnsTrue_When_Valid_targetID()
	{
		$t = new Target();
		$set = $t->setTarget(1);
		
		$this->assertTrue($set);
	}
	
	public function testSetTarget_ReturnsFalse_When_Invalid_TargetID()
	{
		$t = new Target();
		$set = $t->setTarget(3);
		
		$this->assertFalse($set);
	}
	
	public function testCleanTarget_NukesDB()
	{
		$this->createTestTable();
		$t = new Target();
		$t->setTarget(1);
		
		$t->cleanTarget();
		$this->assertFalse($this->checkTestTableExists());
	}
	
	public function testCleanTarget_RemovesFiles_from_targetDirectory()
	{
		$this->createTestFile();
		if (!$this->checkTestFileExists() ) {
			return false;
		}
		
		$t = new Target();
		$set = $t->setTarget(1);
		if (!$set) {
			return false;
		}
		
		$t->cleanTarget();
		
		$this->assertFalse($this->checkTestFileExists());
	}
	
	public function testLoadDB_loads_database_to_targetDB()
	{
		$t = new Target();
		$t->setTarget(1);
		$t->cleanTarget();
		$result = $t->loadDb(dirname(__FILE__).'/../_files/create_testLoadTable.sql');
		if (! $result) {
			return false;
		}
		
		
		$this->assertTrue($this->checkTestLoadTableExists() );
	}
	
	public function testLoadDB_nonexistentFile_returns_false()
	{
		$t = new Target();
		$t->setTarget(1);
		$result = $t->loadDb('thisisnotarealfile.sql.txt.php.html.txt');
		
		$this->assertFalse($result);
	}
	
	
	public function testGetAvailableTarget_returns_targetID_when_availableTarget_found()
	{
		$t = new Target();
		$targetID = $t->getAvailableTarget();
		$this->assertEquals(1, $targetID);
	}
	
	public function testGetAvailableTarget_returnsFalse_when_noAvailableTargetFound()
	{
		$this->setAllTargetsToLocked();
		
		$t = new Target();
		$targetID = $t->getAvailableTarget();
		$this->assertFalse($targetID);
	}
	
	
	public function testCheckNukeDbNeeded_Returns_true_when_TableExists()
	{
		$this->createTestTable();
		$t = new Target();
		$t->setTarget(1);
		$needed = $t->checkNukeDbNeeded();
		$this->assertTrue($needed);
	}
	
	public function testCheckNukeDbNeeded_Returns_false_when_NoTablesExist()
	{
		$this->createTestTable();
		$t = new Target();
		$t->setTarget(1);
		$t->cleanTarget();
		$needed = $t->checkNukeDbNeeded();
		$this->assertFalse($needed);
	}
	
	public function testUnlockTarget_Sets_targetLocked_False()
	{
		$t = new Target();
		$t->setTarget(1);
		$t->cleanTarget();
		$t->unlockTarget();
		
		$locked = $this->checkTargetLocked(1);
		
		$this->assertFalse($locked);
	}
	
	
	private function createTestTable()
	{
		$dbname = 'www_test2';
		$username = 'www_test2';
		$password = '1mmmn9TFb7pk3zZ';
		$conn = new \PDO("mysql:host=localhost;dbname=$dbname", $username, $password);
		$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

		$stmt = $conn->prepare('CREATE TABLE IF NOT EXISTS `test` (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY);');
		$stmt->execute();
	}
	
	private function checkTestTableExists()
	{
		$dbname = 'www_test2';
		$username = 'www_test2';
		$password = '1mmmn9TFb7pk3zZ';
		$conn = new \PDO("mysql:host=localhost;dbname=$dbname", $username, $password);
		$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		
		$stmt = $conn->prepare("SHOW TABLES LIKE 'test'; ");
		if ( $stmt->rowCount() > 0 ) {
			return false;
		}
		
		return false;
	}
	
	private function checkTestLoadTableExists()
	{
		$dbname = 'www_test2';
		$username = 'www_test2';
		$password = '1mmmn9TFb7pk3zZ';
		$conn = new \PDO("mysql:host=localhost;dbname=$dbname", $username, $password);
		$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		
		$stmt = $conn->prepare('SELECT id FROM testLoad; ');
		$stmt->execute();
		if ( $stmt->rowCount() > 0 ) {
			return true;
		}
		
		return false;
	}
	
	private function createTestFile()
	{
		$filePath = '/var/www/test2/unitTestGenerated.txt';
		file_put_contents($filePath, 'File Auto-generated by unit tests!');
	}
	
	private function checkTestFileExists()
	{
		$filePath = '/var/www/test2/unitTestGenerated.txt';
		if (file_exists($filePath) ) {
			return true;
		}
		return false;
	}
	
	private function setAllTargetsToLocked()
	{
		$dbname = $this->config->dbName;
		$username = $this->config->dbUser;
		$password = $this->config->dbPassword;
			
		$conn = new \PDO("mysql:host=localhost;dbname=$dbname", $username, $password);
		$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		
		$stmt = $conn->prepare('UPDATE `target` SET locked = 1; ');
		$stmt->execute();	
	}
	
	private function setAllTargetsToUnlocked()
	{
		$dbname = $this->config->dbName;
		$username = $this->config->dbUser;
		$password = $this->config->dbPassword;
			
		$conn = new \PDO("mysql:host=localhost;dbname=$dbname", $username, $password);
		$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		
		$stmt = $conn->prepare('UPDATE `target` SET locked = 0; ');
		$stmt->execute();	
	}
	
	private function checkTargetLocked($targetID)
	{
		$dbname = $this->config->dbName;
		$username = $this->config->dbUser;
		$password = $this->config->dbPassword;
		
		$conn = new \PDO("mysql:host=localhost;dbname=$dbname", $username, $password);
		$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		
		$stmt = $conn->prepare("SELECT locked FROM target WHERE id = :targetID ");
		$stmt->bindParam(':targetID', $targetID, \PDO::PARAM_INT);
		$stmt->execute();
		
		if ($stmt->rowCount() == 0 ) {
			return false;
		}
		
		$result = $stmt->fetch(\PDO::FETCH_ASSOC);
		$locked = $result['locked'];
		if ($locked == 1) {
			return true;
		}
		
		return false;
	}
	
}