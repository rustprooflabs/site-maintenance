<?php
namespace SiteMaintenance;
require_once(dirname(__FILE__) . '/../../app/system.php');

class systemTest Extends \PHPUnit_Framework_TestCase
{	
	private $logfile;
	
	public function __construct()
	{
		$this->logfile = 'test.log';
	}
	
	
	public function testSystem_WriteLog_Creates_Log_file()
	{
		if (file_exists($this->logfile)) {
			unlink($this->logfile);
		}
		
		$system = new System();
		$system->writeLog("This is a test message.", $this->logfile );
		$this->assertTrue(file_exists($this->logfile ));
		
	}
	
	
	public function testSystem_getFilesize_ReturnsSizeInMB()
	{
		$filepath = "test.txt";
		$cmd = "dd if=/dev/urandom of=$filepath bs=1M count=2";
		$output = array();
		$status = null;
		exec ($cmd, $output, $status);
		
		$system = new System();
		$filesize = $system->getFileSize($filepath);
		$this->assertEquals(2, $filesize);
	}
	
	
	public function testSystem_getFilesize_ReturnsSizeInGB()
	{
		$filepath = "test.txt";
		$cmd = "dd if=/dev/urandom of=$filepath bs=3M count=4";
		$output = array();
		$status = null;
		exec ($cmd, $output, $status);
		
		$system = new System();
		$filesize = $system->getFileSize($filepath, "GB");
		$this->assertEquals(0.01171875, $filesize);
	}
	
	public function testSystem_getFilesize_InvalidUnits_ReturnsFalse()
	{
		$system = new System();
		$filepath = "test.txt";
		$filesize = $system->getFileSize($filepath, "NotReal");
		$this->assertFalse($filesize);
	}
	
	public function testSystem_CreateDir_Creates_Dir()
	{
		$system = new System();
		$dir = 'deleteme/';
		
		if (file_exists($dir)) {
			rmdir($dir);
		}
		
		$system->createDir($dir);
		
		$this->assertTrue(file_exists($dir));
	}

}