<?php
namespace SiteMaintenance;
require_once(dirname(__FILE__) . '/../../app/archive.php');
require_once(dirname(__FILE__) . '/../../app/backup.php');
require_once(dirname(__FILE__) . '/../../app/site.php');
require_once(dirname(__FILE__) . '/../../SiteMaintenanceConfig.php');

class archiveTest Extends \PHPUnit_Extensions_Database_TestCase
{	
	private $config;
	private $logfile;
	
	public function __construct() {
		$this->config = new SiteMaintenanceConfig();
		$this->logfile = 'backup.log';
	}
	
	
	public function getConnection()
    {
		$config = $this->config;
		$dbname = $config->dbName;
		$username = $config->dbUser;
		$password = $config->dbPassword;
		
        $pdo = new \PDO("mysql: host=localhost; dbname=$dbname", $username, $password);
        return $this->createDefaultDBConnection($pdo, $dbname);
    }
	
	public function getDataSet()
	{
		return $this->createXMLDataSet(dirname(__FILE__).'/../_files/site_single_record.xml');
	}
	
	public function testArchive_createArchive_noVariable_ReturnsFalse()
	{
		$archive = new Archive();
		$status = $archive->createArchive();
		$this->assertFalse($status);
	}
	
	public function testArchive_runBackupProcess_createsSiteArchiveRecord()
	{
		
		$backup = new Backup();
		$site = new Site();
		$archive = new Archive();
		
		$s = $site->get(1);
		$schedules = $backup->getSchedules(1);
		
		foreach ($schedules as $schedule) {
			$backup->setSchedule($s, $schedule);
			if ($backup->checkBackupNeeded() ) {
					$status = $backup->backup();
				}
		}
		
		$this->assertTrue($status);	
	}
	
	public function testArchive_getArchive_ScheduleID_ReturnsFullFilepath_RecentBackup()
	{
		$this->assertTrue(false);
	}
	
	public function testArchive_extractArchive_ReturnsTrue_WhenSuccessful()
	{
		$t = new Target();
		$a = new Archive();
		$archivePath = dirname(__FILE__).'/../_files/testSite2Demo.zip';
		
		$t->setTarget(1);
		$targetPath = $t->getTargetPath();
		$extracted = $a->extractArchive($archivePath, $targetPath);
		
		$checkFile = $targetPath . '/index.html';
		$indexFileExists = file_exists($checkFile);
		$this->assertTrue($indexFileExists, 'File does not exist:  ' . $checkFile);
	}
	
	public function testGetRecentArchive_ReturnsArray_OnSuccess()
	{
		$a = new Archive();
		$archive = $a->getRecentArchive(1);
		$expected = array("not real" => "No this text is not expected....");
		$this->assertEquals($expected, $archive);
	}
	
}