<?php
namespace SiteMaintenance;
require_once(dirname(__FILE__) . '/../../app/encryption/encrypt.php');
require_once(dirname(__FILE__) . '/../../app/encryption/decrypt.php');
require_once(dirname(__FILE__) . '/../../SiteMaintenanceConfig.php');

class encryptTest Extends \PHPUnit_Framework_TestCase
{	
	private $encryptedFile = 'unitTest.enc';
	private $stringToEncrypt = 'pleaseEncrypt Me!';
	
	public function testEncryptingStringReturnsTrueOnSuccess()
	{
		$config = new SiteMaintenanceConfig();
		$encrypt = new encrypt();
		
		$stringToEncrypt = $this->stringToEncrypt;
		$outputFile = $this->encryptedFile;
		$pubKey = $config->keyPublicDecrypt;
		$encrypt->setVars($stringToEncrypt, $outputFile, $pubKey);
		$encrypted = $encrypt->encrypt();
		
		$this->assertTrue($encrypted);
	}
	
	
	public function testEncryptStringCreatesEncryptedFile()
	{
		$config = new SiteMaintenanceConfig();
		$encrypt = new encrypt();
		
		$stringToEncrypt = $this->stringToEncrypt;
		$outputFile = $this->encryptedFile;
		$pubKey = $config->keyPublicDecrypt;
		$encrypt->setVars($stringToEncrypt, $outputFile, $pubKey);
		$encrypted = $encrypt->encrypt();
		
		$this->assertFileExists( $outputFile );
	}
	
	
	public function testEncryptStringCreatesFileDecryptClassCanDecrypt()
	{
		$config = new SiteMaintenanceConfig();
		$encrypt = new encrypt();
		$decrypt = new decrypt();
		
		$stringToEncrypt = $this->stringToEncrypt;
		$outputFile = $this->encryptedFile;
		$pubKey = $config->keyPublicDecrypt;
		$encrypt->setVars($stringToEncrypt, $outputFile, $pubKey);
		$encrypt->encrypt();
		
		$decryptedString = $decrypt->execute($outputFile);
		
		$this->assertEquals($stringToEncrypt, $decryptedString);
	}
	
}
?>