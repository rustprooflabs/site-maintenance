<?php
namespace SiteMaintenance;
require_once(dirname(__FILE__) . '/../../app/backup.php');
require_once(dirname(__FILE__) . '/../../app/site.php');
require_once(dirname(__FILE__) . '/../../SiteMaintenanceConfig.php');

class backup_remoteHost_test Extends \PHPUnit_Extensions_Database_TestCase
{	
	
	private $config;
	private $backupNeeded;
	private $fileBackup;
	private $dbBackup;
	
	public function __construct() 
	{
		$this->config = new SiteMaintenanceConfig();
	}
	
	public function getConnection()
    {
		$config = $this->config;
		$dbname = $config->dbName;
		$username = $config->dbUser;
		$password = $config->dbPassword;
		
        $pdo = new \PDO("mysql: host=localhost; dbname=$dbname", $username, $password);
        return $this->createDefaultDBConnection($pdo, $dbname);
    }
	
	public function getDataSet()
	{
		return $this->createXMLDataSet(dirname(__FILE__).'/../_files/site_remoteHost.xml');
	}
	
	public function testBackup_Backup_RemoteHost_FilesAndDb_Creates_Archive()
	{
		if (file_exists($this->config->backupPath . '/testSite1_archive.zip') ) {
			unlink ($this->config->backupPath . '/testSite1_archive.zip');
		}
		
		$backup = new Backup();
		$site = new Site();
		$s = $site->get(1);
		$schedules = $backup->getSchedules(1);
		
		foreach ($schedules as $schedule) {
			$backup->setSchedule($s, $schedule);
			if ($backup->checkBackupNeeded() ) {
					$status = $backup->backup();
				}
		}
		
		$this->assertTrue($status);
		
	}
}