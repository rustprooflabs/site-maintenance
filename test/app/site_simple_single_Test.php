<?php
namespace SiteMaintenance;
require_once(dirname(__FILE__) . '/../../app/site.php');
require_once(dirname(__FILE__) . '/../../SiteMaintenanceConfig.php');

class Site_Simple_Single_Test Extends \PHPUnit_Extensions_Database_TestCase
{	
	private $config;
	
	public function __construct()
	{
		$this->config = new SiteMaintenanceConfig();
	}
	
	public function getConnection()
    {
		$config = $this->config;
		$dbname = $config->dbName;
		$username = $config->dbUser;
		$password = $config->dbPassword;
		
        $pdo = new \PDO("mysql: host=localhost; dbname=$dbname", $username, $password);
        return $this->createDefaultDBConnection($pdo, $dbname);
    }
	
	public function getDataSet()
	{
		return $this->createXMLDataSet(dirname(__FILE__).'/../_files/site_single_record.xml');
	}
	
	public function testDataSourceCreatesOneRowInSourceTable()
    {
        $this->assertEquals(1, $this->getConnection()->getRowCount('site'), "Dataset row not loaded proplerly");
    }
	
	public function testDataSourceCreatesOneRowInSiteBackupCollectionTable()
    {
        $this->assertEquals(1, $this->getConnection()->getRowCount('siteSchedule'), "Dataset row not loaded proplerly");
    }
	
	public function testSiteGetAllReturnsRecord()
	{
		$site = new Site();
		$sites = $site->getAll();
		
		$expected = array(
			array("id" => "1", "systemName" => "testSite1", 
				"displayName" => "System Generated Test Site 1", 
				"siteType" => "NONE", "sourceLocal" => "1",
				"remoteHost" => "localhost", "username" => "jenkins"
				)
			);
		$this->assertEquals($expected, $sites, "Returned Results from getAll not expected results.");
	}
	
	public function testSiteGetReturnsSingleRecord()
	{
		$site = new Site();
		$s = $site->get(1);
		
		$expected = array("id" => "1", "systemName" => "testSite1", 
				"displayName" => "System Generated Test Site 1", 
				"siteType" => "NONE", "sourceLocal" => "1",
				"remoteHost" => "localhost", "username" => "jenkins"
			);
		$this->assertEquals($expected, $s, "Returned Results from get not expected results.");
	}
	
	public function testSiteGet_IdNotValid_ReturnsFalse()
	{
		$site = new Site();
		$s = $site->get(-1);
		
		$this->assertFalse($s);
	}
	
	public function testSite_RunBackup_SiteNotSet_ThrowsException()
	{
		$site = new Site();
		$this->setExpectedException('Exception','Site was not set before executing runBackup.');
		$site->runBackup();
	}
	
	public function testSite_RunBackup_SiteSet_RunsSuccessfully()
	{
		$site = new Site();
		$s = $site->get(1);
		$site->set($s);
		$result = $site->runBackup();
		
		$this->assertEquals(1, $result);
	}

	public function testSite_RunBackupTwice_SkipsSecondBackup()
	{
		$site = new Site();
		$s = $site->get(1);
		$site->set($s);
		$site->runBackup();
		$result2 = $site->runBackup();
		
		$this->assertEquals(0, $result2);
	}
}