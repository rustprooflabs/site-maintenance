<?php
namespace SiteMaintenance;
require_once(dirname(__FILE__) . '/../../app/backup.php');
require_once(dirname(__FILE__) . '/../../app/site.php');
require_once(dirname(__FILE__) . '/../../SiteMaintenanceConfig.php');

class backup_Simple_Single_Test Extends \PHPUnit_Extensions_Database_TestCase
{	
	private $config;
	private $backupNeeded;
	private $fileBackup;
	private $dbBackup;
	
	public function __construct() {
		$this->config = new SiteMaintenanceConfig();
	}
	
	public function getConnection()
    {
		$config = $this->config;
		$dbname = $config->dbName;
		$username = $config->dbUser;
		$password = $config->dbPassword;
		
        $pdo = new \PDO("mysql: host=localhost; dbname=$dbname", $username, $password);
        return $this->createDefaultDBConnection($pdo, $dbname);
    }
	
	public function getDataSet()
	{
		return $this->createXMLDataSet(dirname(__FILE__).'/../_files/site_single_record.xml');
	}
	
	public function testBackup_getSchedules_InvalidID_ReturnsFalse()
	{
		$backup = new Backup();
		$schedules = $backup->getSchedules(-1);
		$this->assertFalse($schedules);
		
	}
	
	public function testBackup_getSchedules_ValidID_ReturnsRecord()
	{
		$backup = new Backup();
		$schedules = $backup->getSchedules(1);
		$expected = array(
			array("id" => "1", "backupType" => "BOTH", "backupPath" => "/var/www/testBackup/", 
				"backupFreq" => "1", "lastBackup" => null, "archiveSize" => null, 
				"databaseType" => "MySQL", 
				"siteDbUsername" => "www_test1", "siteDbName" => "www_test1", 
				"siteDbEncryptedPasswordPath" => "/var/maintenance/siteMaintenanceHelpers/dbPasswords/www_test1_db_pw.enc",
				"daysSinceBackup" => null
				)
			);
		$this->assertEquals($expected,$schedules,"The returned schedules did not match the expected values.");
	}
	
	public function testBackup_SetSchedule_LastBackupIsNULL_SetsBackupNeeded_True()
	{
		$backup = new Backup();
		$site = new Site();
		
		$s = $site->get(1);
		$schedules = $backup->getSchedules(1);
		
		foreach ($schedules as $schedule) {
			$backup->setSchedule($s, $schedule);
			$backupNeeded = $backup->checkBackupNeeded();
			$this->assertTrue($backupNeeded);
		}
	}
	


	public function testBackup_Backup_NotSet_Returns_False()
	{
		$backup = new Backup();
		$return = $backup->backup();
		$this->assertFalse($return);
	}
		
}