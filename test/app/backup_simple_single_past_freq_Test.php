<?php
namespace SiteMaintenance;
require_once(dirname(__FILE__) . '/../../app/backup.php');
require_once(dirname(__FILE__) . '/../../SiteMaintenanceConfig.php');

class backup_Simple_Single_Past_Freq_Test Extends \PHPUnit_Extensions_Database_TestCase
{	
	private $config;
	private $backupNeeded;
	private $fileBackup;
	private $dbBackup;
	
	public function __construct() {
		$this->config = new SiteMaintenanceConfig();
	}
	
	public function getConnection()
    {
		$config = $this->config;
		$dbname = $config->dbName;
		$username = $config->dbUser;
		$password = $config->dbPassword;
		
        $pdo = new \PDO("mysql: host=localhost; dbname=$dbname", $username, $password);
        return $this->createDefaultDBConnection($pdo, $dbname);
    }
	
	public function getDataSet()
	{
		return $this->createXMLDataSet(dirname(__FILE__).'/../_files/site_single_record_past_freq.xml');
	}
	
	
	public function testBackup_SetSchedule_LastBackupOverFrequency_SetsBackupNeeded_True()
	{
		$backup = new Backup();
		$site = new Site();
		$s = $site->get(1);
		$schedules = $backup->getSchedules(1);
		foreach ($schedules as $schedule) {
			$backup->setSchedule($s, $schedule);
			$backupNeeded = $backup->checkBackupNeeded();
			$this->assertTrue($backupNeeded);	
		}
	}

	
	
}