<?php
namespace SiteMaintenance;
require_once(dirname(__FILE__) . '/../../app/encryption/decrypt.php');

class decryptTest Extends \PHPUnit_Framework_TestCase
{	
		
	public function testDecryptExecuteReturnsExceptionWhenNoInfilePassed()
	{
		$decrypt = new decrypt();
		$this->setExpectedException('Exception','No file passed to decrypt.');
		$decrypt->execute();
	}
	
	public function testDecryptExecuteReturnsFalseWhenBadFilename()
	{
		$decrypt = new decrypt();
		$notAFile = 'this_will_never_be_a_filename.enc';
		$this->setExpectedException('Exception','File passed is not a valid file.');
		$decrypt->execute($notAFile);
	}
	
	public function testEncryptedPasswordFileExists()
	{
		$path = '/var/maintenance/scripts/dbPasswords/devwebJenkins';
		$validFile = 'devweb_test1_db_pw.enc';
		$validPath = $path . '/' . $validFile;
		$this->assertFileExists( $validPath);
	}
	
	public function testDecryptExecuteReturnsDecryptedStringWhenGoodFilenameAndKey()
	{
		$decrypt = new decrypt();
		
		$path = '/var/maintenance/scripts/dbPasswords/rpldev01';
		$validFile = 'www_test1_db_pw.enc';
		$validPath = $path . '/' . $validFile;
		
		$string = $decrypt->execute($validPath);
		echo "String:  $string \n";
		$this->assertGreaterThan(0, strlen($string));
	}
	
	public function testDecryptExecuteReturnsEmptyStringWhenGoodFilenameWrongKey()
	{
		$decrypt = new decrypt();
		
		$path = '/var/maintenance/scripts/dbPasswords/bcsocean';
		$validFile = 'vadebonclaw_db_pw.enc';
		$validPath = $path . '/' . $validFile;
		
		$nothing = $decrypt->execute($validPath);
		$this->assertEquals($nothing, '');
	}

	
}

?>