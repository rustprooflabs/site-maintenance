<?php
namespace SiteMaintenance;
require_once(dirname(__FILE__) . '/../../app/RestoreArchive.php');
require_once(dirname(__FILE__) . '/../../SiteMaintenanceConfig.php');

class RestoreArchive_Test Extends \PHPUnit_Extensions_Database_TestCase
{	
	private $config;
	
	public function __construct()
	{
		$this->config = new SiteMaintenanceConfig();
	}
	
	public function getConnection()
    {
		$config = $this->config;
		$dbname = $config->dbName;
		$username = $config->dbUser;
		$password = $config->dbPassword;
		
        $pdo = new \PDO("mysql: host=localhost; dbname=$dbname", $username, $password);
        return $this->createDefaultDBConnection($pdo, $dbname);
    }
	
	public function getDataSet()
	{
		return $this->createXMLDataSet(dirname(__FILE__).'/../_files/site_single_record.xml');
	}

	public function testRestoreArchive_ReturnsTrue_OnSuccess()
	{
		$ra = new RestoreArchive();
		$restored = $ra->restoreArchive(1);
		
		
		$this->assertTrue($restored);
	
	}
	
	
}