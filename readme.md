# RustProof Labs - Site Maintenance README

***

## COPYRIGHT AND DISCLAIMER

Copyright (C) RustProof Labs / rustprooflabs@gmail.com

This program is free software licensed under The MIT License, you should have
received a copy of The MIT License included with this program.

***

## CONTACT AND SUPPORT

Contact Ryan Lambert at rustprooflabs@gmail.com for more details.  In the future
a Wiki will be provided, but is currently out of date and inaccurate.

***

## PURPOSE


The goal of this project is to create a reliable and affordable web site 
backup system that can run effectively on budget hardware.