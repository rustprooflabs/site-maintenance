CREATE TABLE `template` (
	id					INT	AUTO_INCREMENT
	, templateName		VARCHAR(50)
	, templateUserID	INT
	, templateSourceID	INT
	, templateCreated	DATETIME
	, templateDesc		VARCHAR(1000)
	, PRIMARY KEY (`id`)
);
