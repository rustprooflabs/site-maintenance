CREATE TABLE `bkup_collect_site` (
	id					INT	AUTO_INCREMENT
	, username			VARCHAR(100)
	, remoteHost		VARCHAR(100)
	, backupPath		VARCHAR(200)
	, backupRootName	VARCHAR(100)
	, PRIMARY KEY (`id`)
);
