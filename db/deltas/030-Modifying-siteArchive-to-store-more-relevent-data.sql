ALTER TABLE `siteArchive` DROP `filepath`;
ALTER TABLE `siteArchive` ADD dailyBackup BIT NOT NULL DEFAULT b'0' AFTER dbType;
ALTER TABLE `siteArchive` ADD weeklyBackup BIT NOT NULL DEFAULT b'0' AFTER dbType;
ALTER TABLE `siteArchive` ADD monthlyBackup BIT NOT NULL DEFAULT b'0' AFTER dbType;