CREATE TABLE `transfer` (
	id					INT	AUTO_INCREMENT
	, status		VARCHAR(20)	NOT NULL
	, userID		INT			NOT NULL
	, createdDate	DATETIME	NOT NULL
	, statusDate	DATETIME	NOT NULL
	, sourceID		INT			NOT NULL
	, PRIMARY KEY (`id`) 
);

CREATE INDEX transferCreatedDate ON `transfer` (`createdDate`);
CREATE INDEX transferSourceID ON `transfer` (`sourceID`);