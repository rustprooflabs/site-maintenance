DROP TABLE `person`;
DROP TABLE `build`;
DROP VIEW `vAllEligibleSources`;
RENAME TABLE `source` TO `site`;
RENAME TABLE `siteBackupCollection` TO `siteSchedule`;
CREATE TABLE `siteArchive`
(
	id	INT	AUTO_INCREMENT,
	siteID INT NOT NULL,
	siteScheduleID INT NOT NULL,
	created DATETIME NOT NULL,
	archiveSize DECIMAL(14,2) NOT NULL,
	filename VARCHAR(150) NOT NULL,
	filepath VARCHAR(200) NOT NULL,
	siteType VARCHAR(20) NOT NULL,
	dbType VARCHAR(20) NULL,
	PRIMARY KEY (`id`)
);