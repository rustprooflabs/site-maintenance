ALTER TABLE `source` ADD `username` VARCHAR(100) NULL AFTER remoteHost;
UPDATE source s INNER JOIN siteBackupCollection sbc ON s.id = sbc.sourceID SET s.username = sbc.username;
ALTER TABLE `siteBackupCollection` DROP username;