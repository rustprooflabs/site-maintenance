CREATE TABLE person 
(
	id				INT		AUTO_INCREMENT
	, username		VARCHAR(50)		NOT NULL
	, password		CHAR(64)	NOT NULL
	, PRIMARY KEY (`id`)
	, UNIQUE (`username`)
);

INSERT INTO person
SELECT id, username, password FROM buildUser;

DROP TABLE buildUser;