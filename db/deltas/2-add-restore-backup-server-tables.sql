

CREATE TABLE buildUser 
(
	id				INT		AUTO_INCREMENT
	, username		VARCHAR(50)		NOT NULL
	, password		CHAR(64)	NOT NULL
	, lastLogin		DATETIME		NULL
	, PRIMARY KEY (`id`)
	, UNIQUE (`username`)
);




CREATE TABLE `target`
(
	id				INT		AUTO_INCREMENT
	, systemName	VARCHAR(100)	NOT NULL
	, displayName	VARCHAR(100)	NOT NULL
	, targetDesc	VARCHAR(500)	NULL
	, PRIMARY KEY (`id`)
	, UNIQUE (`systemName`)
	, UNIQUE (`displayName`)
);




CREATE TABLE `source`
(
	id				INT		AUTO_INCREMENT
	, systemName	VARCHAR(100)	NOT NULL
	, displayName	VARCHAR(100)	NOT NULL
	, archiveSize	NUMERIC(8,1)	NULL
	, sourceDate	DATETIME		NOT NULL
	, sourceDesc	VARCHAR(500)	NULL
	, PRIMARY KEY (`id`)
	, UNIQUE (`systemName`, `sourceDate`)
);




CREATE TABLE `build`
(
	id				INT		AUTO_INCREMENT
	, status		VARCHAR(20)	NOT NULL
	, userID		INT			NOT NULL
	, createdDate	DATETIME	NOT NULL
	, statusDate	DATETIME	NOT NULL
	, sourceID		INT			NOT NULL
	, targetID		INT			NOT NULL
	, buildDuration	INT			NULL
	, totalDuration	INT			NULL
	, PRIMARY KEY (`id`) 
);

CREATE INDEX buildCreatedDate ON `build` (`createdDate`);
CREATE INDEX buildSource ON `build` (`sourceID`);
CREATE INDEX buildTarget ON `build` (`targetID`);


