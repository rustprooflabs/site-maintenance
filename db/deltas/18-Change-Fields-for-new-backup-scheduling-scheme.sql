ALTER TABLE `siteBackupCollection` DROP `backupHour`;
ALTER TABLE `siteBackupCollection` ADD `backupFreq` INT NOT NULL DEFAULT 7 AFTER `backupPath`;