ALTER TABLE `siteBackupCollection` ADD `archiveSize` DECIMAL(10,2) NULL AFTER last_backup;
ALTER TABLE `source` DROP `archiveSize`;