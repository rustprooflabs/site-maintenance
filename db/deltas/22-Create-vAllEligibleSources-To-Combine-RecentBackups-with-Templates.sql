CREATE VIEW vAllEligibleSources AS
SELECT CONCAT('r', CAST(s.id AS CHAR)) as allSourcesID, s.displayName
    FROM source s
    INNER JOIN siteBackupCollection sbc ON s.id = sbc.sourceID
    WHERE sbc.archiveSize > 0
UNION
SELECT CONCAT( 't', CAST( t.id AS CHAR ) ) AS allSourcesID, t.templateName
    FROM template t;