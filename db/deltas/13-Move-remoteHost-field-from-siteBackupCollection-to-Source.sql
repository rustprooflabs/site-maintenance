ALTER TABLE `source` ADD `remoteHost` VARCHAR(100) NULL AFTER displayName;
UPDATE source s INNER JOIN siteBackupCollection sbc ON s.id = sbc.sourceID SET s.remoteHost = sbc.remoteHost;
ALTER TABLE `siteBackupCollection` DROP remoteHost;