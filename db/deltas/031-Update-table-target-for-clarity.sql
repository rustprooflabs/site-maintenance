ALTER TABLE `target` DROP `pathTmp`;
ALTER TABLE `target` DROP `pathLog`;
ALTER TABLE `target` CHANGE `systemName` `name` VARCHAR(100);
ALTER TABLE `target` CHANGE `targetDirectory` `filePath` VARCHAR(200);