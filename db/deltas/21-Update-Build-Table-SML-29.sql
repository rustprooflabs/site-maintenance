ALTER TABLE `build` ADD `buildType` VARCHAR(20) NOT NULL DEFAULT 'recentbackup' AFTER `id`;
ALTER TABLE `build` ADD `buildSourceID` INT NULL AFTER `buildType`;
UPDATE `build` SET `buildSourceID` = `sourceID`;
ALTER TABLE `build` MODIFY `buildSourceID` INT NOT NULL;
ALTER TABLE `build` DROP `sourceID`;