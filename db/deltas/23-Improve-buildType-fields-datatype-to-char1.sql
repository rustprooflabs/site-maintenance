UPDATE `build` SET `buildType` = 'r' WHERE buildType = 'recentbackup';
UPDATE `build` SET `buildType` = 't' WHERE buildType = 'template';
ALTER TABLE `build` MODIFY `buildType` CHAR(1) NOT NULL DEFAULT 'r';