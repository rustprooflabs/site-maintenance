ALTER TABLE `target` ADD targetDirectory VARCHAR(200);
ALTER TABLE `target` ADD dbHost VARCHAR(80);
ALTER TABLE `target` ADD dbName VARCHAR(80);
ALTER TABLE `target` ADD dbUser VARCHAR(80);
ALTER TABLE `target` ADD dbPassword VARCHAR(200);

ALTER TABLE `source` ADD dbPrefix VARCHAR(10);