<?php
namespace SiteMaintenance;

require_once(dirname(__FILE__) . '/../SiteMaintenanceConfig.php');

class System
{
	
	public function getTimestamp($format = 'MySQL')
	{
		if ($format == "MySQL") {
			return date("Y-m-d H:i:s",time());
		}
		elseif ($format =='Filename') {
			return date("Y-m-d--H_i",time());
		}
		else {
			return date("Y-m-d",time());
		}
	}
	
	public function writeLog( $msg, $logfile, $status = 'INFO' )
	{
		$logMsg = $this->getTimestamp() . ' -- ' . $status . ' -- ';
		$logMsg .= $msg . "\n";
		file_put_contents($logfile, $logMsg, FILE_APPEND);
	}
	
	public function createDir( $dir )
	{
		if (!file_exists($dir)) {
			mkdir($dir);
		}
	}
	
	public function removeDir( $dir )
	{
		if (!file_exists($dir)) {
			return true;
		}

		if (!is_dir($dir)) {
			return unlink($dir);
		}

		foreach (scandir($dir) as $item) {
			if ($item == '.' || $item == '..') {
				continue;
			}

			if (!$this->removeDir($dir . DIRECTORY_SEPARATOR . $item)) {
				return false;
			}

		}

		return rmdir($dir);
	}
	
	public function moveFile( $source, $destination )
	{
		
	}
	
	
	public function getFileSize($filepath, $units = 'MB')
	{
		$filesize = filesize($filepath);
		if ($units == 'MB') {
			$filesize = $filesize / 1024 / 1024;
		}
		else if ($units == 'GB') {
			$filesize = $filesize / 1024 / 1024 / 1024;
		}
		else {
			return false;
		}
		
		return $filesize;
	}
	
}