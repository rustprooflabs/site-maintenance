<?php
namespace SiteMaintenance;

require_once(dirname(__FILE__) . '/../SiteMaintenanceConfig.php');
require_once(dirname(__FILE__) . '/target.php');
require_once(dirname(__FILE__) . '/archive.php');
require_once(dirname(__FILE__) . '/system.php');

class RestoreArchive
{
	
	private $config;
	private $system;
	private $logfile;
	
	public function __construct()
	{
		$this->config = new SiteMaintenanceConfig();
		$this->system = new System();
		$this->logfile = $this->config->logfile;
	}
	
	public function restoreArchive($siteID)
	{
		$t = new Target();
		$a = new Archive();
		
		$siteArchive = $a->getRecentArchive($siteID);
		
		$backupPath = $this->config->backupPath;
		$archiveName = $siteArchive['filename'];
		$archivePath = $backupPath . '/' . $archiveName;
		
		$targetID = $t->getAvailableTarget();
		
		if (!$targetID) {
			$msg = 'There was no available target to restore to.  File ' . $archivePath . ' NOT restored.';
			$this->system->writeLog($msg, $this->logfile, 'WARNING');
			return false;
		}
		
		$set = $t->setTarget( $targetID );

		if (! $set ) {
			$msg = 'There was a problem setting the target.  File ' . $filepath . ' NOT tested.';
			$this->system->writeLog($msg, $this->logfile, 'WARNING');
			return false;
		}

		$t->cleanTarget();
		$targetPath = $t->getTargetPath();

		if (! $a->extractArchive($archivePath, $targetPath) ) {
			$msg = 'There was a problem extracting the target archive file.  File: ' . $archivePath . ' NOT tested.';
			$this->system->writeLog($msg, $this->logfile, 'WARNING');
			return false;
		}

		$dbLoadStatus = $t->loadDb($targetPath . '/sql/full-db-dump.sql');
		if ($dbLoadStatus > 0 ) {
			$msg = 'There was a problem loading the database for archive: ' . $archivePath  ;
			$this->system->writeLog($msg, $this->logfile, 'WARNING');
			return false;
		}
		
		return true;
	}

}

?>
