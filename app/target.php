<?php
namespace SiteMaintenance;

require_once(dirname(__FILE__) . '/../SiteMaintenanceConfig.php');
require_once(dirname(__FILE__) . '/system.php');

class Target
{
	private $config;
	private $target;
	private $system;
	private $dropFile;
	private $logfile;
	
	public function __construct()
	{
		$this->config = new SiteMaintenanceConfig();
		$this->system = new System();
		$this->logfile = $this->config->logfile;
	}
	
	public function getAvailableTarget()
	{
		$dbname = $this->config->dbName;
		$username = $this->config->dbUser;
		$password = $this->config->dbPassword;
			
		$conn = new \PDO("mysql:host=localhost;dbname=$dbname", $username, $password);
		$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		
		$stmt = $conn->prepare('SELECT id
				FROM `target`
				WHERE locked = 0;
				LIMIT 1;
			');
		$stmt->execute();
		if ($stmt->rowCount() == 0 ) {
			return false;
		}
		$target = $stmt->fetch(\PDO::FETCH_ASSOC);
		$targetID = $target['id'];
		return $targetID;
	}
	
	
	public function getTargets()
	{
		$dbname = $this->config->dbName;
		$username = $this->config->dbUser;
		$password = $this->config->dbPassword;
			
		$conn = new \PDO("mysql:host=localhost;dbname=$dbname", $username, $password);
		$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

		$stmt = $conn->prepare('SELECT id, name, displayName, targetDesc
				FROM `target`
			');
		$stmt->execute();
		$targets = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		return $targets;
	}
	
	
	public function setTarget( $targetID )
	{
		$target = $this->getTarget($targetID);
		if (!$target) {
			return false;
		}
		if ($target['locked'] == 0) {
			$this->lockTarget($targetID);
		}
		
		$this->target = $target;
		return true;
	}
	
	public function getTargetPath()
	{
		$target = $this->target;
		return $target['filePath'];
	}
	
	
	private function getTarget( $targetID )
	{
		$dbname = $this->config->dbName;
		$username = $this->config->dbUser;
		$password = $this->config->dbPassword;
			
		$conn = new \PDO("mysql:host=localhost;dbname=$dbname", $username, $password);
		$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

		$stmt = $conn->prepare('SELECT id, name, displayName, targetDesc,
				filePath, dbHost, dbName, dbUser, dbPassword, locked
			FROM `target`
			WHERE id = :targetID
			');
		$stmt->bindParam(':targetID', $targetID, \PDO::PARAM_INT);
		$stmt->execute();
		if ($stmt->rowCount() == 0 ) {
			return false;
		}
		$target = $stmt->fetch(\PDO::FETCH_ASSOC);
		return $target;
	}
	
	private function lockTarget ( $targetID ) 
	{
		$dbname = $this->config->dbName;
		$username = $this->config->dbUser;
		$password = $this->config->dbPassword;
			
		$conn = new \PDO("mysql:host=localhost;dbname=$dbname", $username, $password);
		$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

		$stmt = $conn->prepare('UPDATE `target` SET locked = 1
			WHERE id = :targetID ;
			');
		$stmt->bindParam(':targetID', $targetID, \PDO::PARAM_INT);
		return $stmt->execute();
	}
	
	public function unlockTarget( )
	{
		$this->cleanTarget();
		$targetID = $this->target['id'];
		
		$dbname = $this->config->dbName;
		$username = $this->config->dbUser;
		$password = $this->config->dbPassword;
			
		$conn = new \PDO("mysql:host=localhost;dbname=$dbname", $username, $password);
		$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

		$stmt = $conn->prepare('UPDATE `target` SET locked = 0
			WHERE id = :targetID ;
			');
		$stmt->bindParam(':targetID', $targetID, \PDO::PARAM_INT);
		return $stmt->execute();
	}
	
	
	public function cleanTarget()
	{
		$msgStart = 'Starting cleaning process for targetID: ' . $this->target['id'];
		$this->system->writeLog($msgStart, $this->logfile);
		
		if ($this->checkNukeDbNeeded() ) {
			$dbClean = $this->nukeDb();
		}
		$filesClean = $this->removeFiles();
	}
	
	private function nukeDb()
	{
		$target = $this->target;
		$this->setDropFile();
		$cmdGenerated = $this->generateNukeDbScript();
		if ($cmdGenerated > 0 ) {
			$msg = 'Nuke DB Failed for target:  ';
			foreach ($target as $i) {
				$msg .= ' ' . $i . ' ';
			}
			$this->system->writeLog($msg, $this->logfile, 'ERROR');
			return false;
		}
		
		$dbHost = $target['dbHost'];
		$dbName = $target['dbName'];
		$dbUser = $target['dbUser'];
		$dbPassword = $target['dbPassword'];
		
		$cmd = null;
		$cmd = 'mysql -u' . $dbUser . ' -p' . $dbPassword . ' ';
		$cmd .= ' -h ' . $dbHost . ' ' . $dbName . ' < ' . escapeshellarg($this->dropFile);

		$output = array();
		$status = null;
		exec($cmd, $output, $status);
		return $status;
		
	}
	
	public function checkNukeDbNeeded()
	{
		$dbHost = $this->target['dbHost'];
		$dbName = $this->target['dbName'];
		$dbUser = $this->target['dbUser'];
		$dbPassword = $this->target['dbPassword'];
		
		$conn = new \PDO("mysql:host=localhost;dbname=$dbName", $dbUser, $dbPassword);
		$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

		$stmt = $conn->prepare('SHOW TABLES;
			');
		
		if (! $stmt->execute() ) {
			return false;
		}
		if ($stmt->rowCount() > 0 ) {
			return true;
		}
		return false;
	}
	
	private function setDropFile()
	{
		$target = $this->target;
		$dbName = $target['dbName'];
		$this->dropFile = dirname(__FILE__) . '/drop-' . $dbName . '.sql';
	}
	
	
	private function generateNukeDbScript()
	{
		$dbHost = $this->target['dbHost'];
		$dbName = $this->target['dbName'];
		$dbUser = $this->target['dbUser'];
		$dbPassword = $this->target['dbPassword'];
		
		$dropFile = $this->dropFile;
		
		$dropBegin = "SET FOREIGN_KEY_CHECKS=0; \n";
		file_put_contents($dropFile, $dropBegin);
		
		$cmd = 'mysqldump -u' . $dbUser . ' -p' . $dbPassword . ' ';
		$cmd .= ' -h ' . $dbHost . ' --add-drop-table --no-data ' . $dbName . ' ';
		$cmd .= ' | grep ^DROP >> ' . escapeshellarg($dropFile);
		
		$output = array();
		$status = null;
		exec($cmd, $output, $status);
		
		if ($status > 0) {
			$msg = 'Failed to generate proper DB Nuke script.  Return Status: ' . $status;
			$msg .= ' -- Output:  ';
			foreach ($output as $i) {
				$msg .= ' ' . $i . ' ';
			}
			$this->system->writeLog($msg, $this->logfile, 'ERROR');
		}
		
		return $status;
	}
	
	private function removeFiles()
	{
		$target = $this->target;
		$targetDir = $target['filePath'];
		
		if (file_exists($targetDir)) {
			$cmd = 'rm -r ' . escapeshellarg($targetDir);
			$output = array();
			$status = null;
			exec($cmd, $output, $status);
		}
		
		$cmd = null;
		$cmd = 'mkdir ' . escapeshellarg($targetDir);
		$output = array();
		$status = null;
		exec($cmd, $output, $status);
			
		if ($status > 0) {
			$msg = 'Failed to recreate target directory.  Return Status: ' . $status;
			$this->system->writeLog($msg, $this->logfile, 'ERROR');
		}
		
		return $status;
	}
	
	public function loadDb($filePath)
	{
		if (!file_exists($filePath )) {
			$msg = 'The SQL Installer file path does not exist.  Filepath: ' . $filePath;
			$this->system->writeLog($msg, $this->logfile, 'ERROR');
			return false;
		}
		
		$target = $this->target;
		$dbUser = $target['dbUser'];
		$dbPassword = $target['dbPassword'];
		$dbHost = $target['dbHost'];
		$dbName = $target['dbName'];
		
		$cmd = 'mysql -u' . $dbUser . ' -p' . $dbPassword . ' -h ' . $dbHost . ' ';
		$cmd .= $dbName . ' < ' . escapeshellarg($filePath) ;
		$output = array();
		$status = null;
		exec($cmd, $output, $status);
		return $status;
	}
	
}