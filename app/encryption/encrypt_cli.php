<?php
namespace SiteMaintenance;
require_once(dirname(__FILE__) . '/encrypt.php');

/**
 *  This script is intended to be ran via CLI.
 * 
 *  Three command line arugments are required to run this script.
 *		1:  String to encrypt (most likely a password)
 *		2:  The target output file to store the encrypted string
 *		3:  The public key to use to encrypt the string.  Only the matching private
 *				key will be able to decrypt the string.
 * 
 *	Example usage:
 *	php encrypt_cli.php "passwordhere" "encrypted_pw.enc" "public_servername.pem"
 * 
 * 
 * Keys used should be created in the format of:
 * openssl genrsa -out private_<sitename>.pem 1024
 * openssl rsa -in private_<sitename>.pem -pubout -out public_<sitename>.pem
 */

class encryptCLI
{

	private $stringToEncrypt;
	private $outFile;
	private $pubKey;

	public function __construct()
	{
		$this->stringToEncrypt = $_SERVER["argv"][1];
		$this->outFile = $_SERVER["argv"][2];
		$this->pubKey = $_SERVER["argv"][3];		// Path to the public key to use to encrypt
	}


	public function encrypt()
	{
		$encrypt = new encrypt();
		$encrypt->setVars($this->stringToEncrypt, $this->outFile, $this->pubKey);
		if ( $encrypt->encrypt() )	{
			echo "String successfully encrypted!\n";
		}
		else {
			echo "There was a problem encrypting the string.\n";
		}
	}

}

$encryption = new encryptCLI();
$encryption->encrypt();

?>