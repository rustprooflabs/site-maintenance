<?php
namespace SiteMaintenance;

require_once(dirname(__FILE__) . '/../../SiteMaintenanceConfig.php');

	class decrypt
	{

		/**
		 *
		 * @param string $infile	Filepath to the encrypted file
		 * @return string	Decrypted text from the file  
		 */
		public function execute ($infile = null)
		{
			$this->checkInfile($infile);
			$decrypted = $this->decryption($infile);
			return $decrypted;
		}
		
		
		/**
		 *
		 * @param string $infile	Filepath to the encrypted file
		 * @return string	Decrypted text from the file 
		 */
		private function decryption ($infile = null)
		{
			$config = new SiteMaintenanceConfig();
			$privKey = $config->keyPrivateDecrypt;
			
			$outfile = 'tmp.txt';
			$cmd = "openssl rsautl -decrypt -inkey $privKey -in $infile -out $outfile";
			
			$output = array();
			$status = null;
		
			exec($cmd, $output, $status);
			
			$decrypted = file_get_contents($outfile);
			unlink($outfile);
			return $decrypted;
			

		}
		
		
		public function checkInfile($infile = null)
		{
			if ($infile == null) {
				throw new \Exception('No file passed to decrypt.');
			}
			
			if (! file_exists($infile) ) {
				throw new \Exception('File passed is not a valid file.');
			}
		}
		
	}

?>