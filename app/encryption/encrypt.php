<?php
namespace SiteMaintenance;

class encrypt
{
	private $stringToEncrypt;
	private $outFile;
	private $pubKey;

	public function setVars($stringToEncrypt, $outfile, $pubKey)
	{
		$this->stringToEncrypt = $stringToEncrypt;
		$this->outFile = $outfile;
		$this->pubKey = $pubKey;
	}
	
	public function encrypt()
	{	
		$tmpFile = 'tmp.txt';

		file_put_contents($tmpFile, $this->stringToEncrypt);

		$cmd = "openssl rsautl -encrypt -inkey $this->pubKey -pubin -in $tmpFile -out $this->outFile ";
		system($cmd);
		unlink($tmpFile);
		return true;
	}

}

?>