<?php
namespace SiteMaintenance;

require_once(dirname(__FILE__) . '/../SiteMaintenanceConfig.php');
require_once(dirname(__FILE__) . '/backup.php');
require_once(dirname(__FILE__) . '/system.php');

class Site
{
	private $config;
	private $site = array();

	public function __construct() 
	{
		$this->config = new SiteMaintenanceConfig();
		$this->logfile = $this->config->logfile;
	}
	
	public function getAll()
	{
		$config = $this->config;
		$dbname = $config->dbName;
		$username = $config->dbUser;
		$password = $config->dbPassword;
			
		$conn = new \PDO("mysql:host=localhost;dbname=$dbname", $username, $password);
		$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

		$stmt = $conn->prepare('SELECT id, systemName, displayName, siteType, 
					sourceLocal, remoteHost, username
				FROM `site`
			');
		$stmt->execute();
		$sites = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		return $sites;
	}
	
	public function get($id)
	{
		$config = $this->config;
		$dbname = $config->dbName;
		$username = $config->dbUser;
		$password = $config->dbPassword;
			
		$conn = new \PDO("mysql:host=localhost;dbname=$dbname", $username, $password);
		$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

		$stmt = $conn->prepare('SELECT id, systemName, displayName, siteType, 
					sourceLocal, remoteHost, username
				FROM `site`
				WHERE id = :siteID
			');
		
		$stmt->bindParam(':siteID', $id, \PDO::PARAM_INT);
		$stmt->execute();
		$site = $stmt->fetch(\PDO::FETCH_ASSOC);
		return $site;
	}
	
	public function set(array $site)
	{
		$this->site = $site;
	}
	
	public function runBackup()
	{
		if( empty($this->site) ) {
			throw new \Exception('Site was not set before executing runBackup.');
		}
		
		$backup = new Backup();
		$system = new System();
		$site = $this->site;
		$siteID = $site['id'];
		$systemName = $site['systemName'];
		$schedules = $backup->getSchedules($siteID);
		
		$cnt = 0;
		
		foreach ($schedules as $schedule) {
			$backup->setSchedule($site, $schedule);
			if ( $backup->checkBackupNeeded() ) {
				$backup->backup();
				$cnt = $cnt + 1;
			}
			else {
				$msg = 'Site:  ' . $systemName . ' had no backups needing to be ran.';
				$system->writeLog($msg, $this->logfile);
			}
		}
		
		return $cnt;
	}
	
}