<?php
namespace SiteMaintenance;

require_once(dirname(__FILE__) . '/../SiteMaintenanceConfig.php');
require_once(dirname(__FILE__) . '/site.php');
require_once(dirname(__FILE__) . '/backup.php');
require_once(dirname(__FILE__) . '/system.php');

class CollectBackups
{
	private $config;
	private $sites;
	
	public function __construct() {
		$this->config = new SiteMaintenanceConfig();
	}
	
	public function execute()
	{
		$system = new System();
		$system->writeLog("----------------", $this->config->logfile);
		$system->writeLog("START OF CollectBackups", $this->config->logfile);
		
		
		$site = new Site();
		
		$this->sites = $site->getAll();
		$this->processSites();
		
		$system->writeLog('--END OF CollectBackups', $this->config->logfile);
		$system->writeLog("----------------", $this->config->logfile);
	}
	
	
	private function processSites()
	{
		$sites = $this->sites;
		$backup = new Backup();
		
		foreach ($sites as $s) {
			$siteID = $s['id'];
			$schedules = $backup->getSchedules($siteID);
			$this->processSiteSchedules($s, $schedules);
		}
	}
	
	
	private function processSiteSchedules( array $site, array $schedules)
	{
		foreach ($schedules as $schedule) {
			$backup = new Backup();
			$backup->setSchedule($site, $schedule);
			if ($backup->checkBackupNeeded() ) {
				$backup->backup();
			}
		}
	}
	
	
	
	
}