<?php
namespace SiteMaintenance;

require_once(dirname(__FILE__) . '/system.php');
require_once(dirname(__FILE__) . '/../SiteMaintenanceConfig.php');

class Archive
{

	private $config;
	private $system;
	private $site;
	private $schedule;
	private $filename;
	private $filepath;
	private $size;
	private $timestampDb;
	private $timestampFile;
	
	public function __construct() {
		$this->config = new SiteMaintenanceConfig();
		$this->system = new System();
	}
	
	
	public function getArchive($scheduleID)
	{
		
	}
	
	public function getRecentArchive($siteID) 
	{
		$config = $this->config;
		$dbname = $config->dbName;
		$username = $config->dbUser;
		$password = $config->dbPassword;
			
		$conn = new \PDO("mysql:host=localhost;dbname=$dbname", $username, $password);
		$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

		$stmt = $conn->prepare('SELECT id AS archiveID, siteID, siteScheduleID, created, archiveSize, filename, siteType, dbType 
				FROM `siteArchive` 
				WHERE siteID = :siteID
				ORDER BY `siteArchive`.`id`  DESC
				LIMIT 1
				;
			');
		
		$stmt->bindParam(':siteID', $siteID, \PDO::PARAM_INT);
		$stmt->execute();
		$archive = $stmt->fetch(\PDO::FETCH_ASSOC);
		return $archive;
	}
	
	public function createArchive(array $site = array(), array $schedule = array() ) 
	{
		if (empty($site) || empty($schedule)) {
			$this->system->writeLog('createArchive method called with empty site or empty schedule.', $this->config->logfile, 'ERROR');
			return false;
		}

		$this->site = $site;
		$this->schedule = $schedule;
		
		$this->setFilepath();
		if (! $this->createZipArchive() ) {
			return false;
		}

		$this->updateArchiveData();
		$filepath = $this->filepath . '/' . $this->filename;
		return $filepath;
	}
	
	private function setFilepath()
	{
		$this->timestampFile = $this->system->getTimestamp('Filename');
		$this->timestampDb = $this->system->getTimestamp();
		
		$siteName = $this->site['systemName'];
		$scheduleID = $this->schedule['id'];
		$archiveName = $siteName . '_' . $scheduleID . '_' . $this->timestampFile . '.zip';
		$this->filename = $archiveName;
		$this->filepath = $this->config->backupPath;
	}
	
	private function createZipArchive()
	{
		$targetArchive = $this->filepath . '/' . $this->filename;

		$sourceDir = 'tmp';
		
		if (! file_exists ($sourceDir)) {
			$this->system->writeLog('The source directory ('.$sourceDir.') does not exist.', $this->config->logfile, 'ERROR');
			return false;
		}
		
		$cmd = "cd $sourceDir && zip -r $targetArchive ./*";
		$output = array();
		$status = null;
		
		exec($cmd, $output, $status);
		if ( $status > 0 ) {
			$this->system->writeLog('There was an error creating the zip archive. Exit status:  ' . $status, $this->config->logfile, 'ERROR');
			return false;
		}
		
		return true;
	}
	
	
	public function extractArchive($archivePath, $targetPath)
	{
		if (!$this->archiveExists($archivePath) ) {
			$this->system->writeLog('The archivePath passed to extractArchive was not valid.', $this->config->logfile, 'ERROR');
			return false;
		}
		
		$cmd = 'unzip ' . escapeshellarg($archivePath) . ' -d ' . escapeshellarg($targetPath)  ;
		
		$output = array();
		$status = null;
		
		exec($cmd, $output, $status);
		if ($status == 9 ) {
			$this->system->writeLog('Extract Archive could not find $archivePath = ' . $archivePath, $this->config->logfile, 'ERROR');
			return false;
		}
		elseif ( $status > 0 ) {
			$this->system->writeLog('There was an error extracting the zip archive. Exit status:  ' . $status, $this->config->logfile, 'ERROR');
			return false;
		}
		
		return true;
	}
	
	private function archiveExists($filepath)
	{
		return file_exists($filepath);
	}
	
	private function updateArchiveData()
	{
		$this->size = $this->system->getFileSize($this->filepath . '/' . $this->filename );
		$this->updateScheduleStats();
		$this->addArchiveRecord();
	}
			
	private function updateScheduleStats()
	{
		$config = $this->config;
		$dbname = $config->dbName;
		$username = $config->dbUser;
		$password = $config->dbPassword;
		$scheduleID = $this->schedule['id'];

		$archiveSize = $this->size;
		$timestamp = $this->timestampDb;
		
		$conn = new \PDO("mysql:host=localhost;dbname=$dbname", $username, $password);
		$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		$sql = 'UPDATE siteSchedule SET last_backup = :timestamp, archiveSize = :archiveSize where id = :scheduleID';
		$stmt = $conn->prepare($sql);
		$stmt->execute(array('timestamp' => $timestamp, 'scheduleID' => $scheduleID, 'archiveSize' => $archiveSize));

	}
	
	private function addArchiveRecord()
	{
		$config = $this->config;
		$dbname = $config->dbName;
		$username = $config->dbUser;
		$password = $config->dbPassword;
		
		$siteID = $this->site['id'];
		$siteScheduleID = $this->schedule['id'];
		$archiveSize = $this->size;
		$timestamp = $this->timestampDb;
		$filename = $this->filename;
		$siteType = $this->site['siteType'];
		$dbType = $this->schedule['databaseType'];
		
		$conn = new \PDO("mysql:host=localhost;dbname=$dbname", $username, $password);
		$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		$sql = 'INSERT INTO siteArchive (siteID, siteScheduleID, created, archiveSize, filename, '
				. ' siteType, dbType, dailyBackup)	'
				. ' VALUES '
				. ' (:siteID, :siteScheduleID, :created, :archiveSize, :filename, '
				. ' :siteType, :dbType, 1)';
		$stmt = $conn->prepare($sql);
		
		$params = array('siteID' => $siteID, 'siteScheduleID' => $siteScheduleID, 
			'created' => $timestamp,'archiveSize' => $archiveSize,'filename' => $filename,
			'siteType' => $siteType,'dbType' => $dbType);
		$stmt->execute($params);
	}
	
}