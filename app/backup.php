<?php
namespace SiteMaintenance;

require_once(dirname(__FILE__) . '/../SiteMaintenanceConfig.php');
require_once(dirname(__FILE__) . '/encryption/decrypt.php');
require_once(dirname(__FILE__) . '/system.php');
require_once(dirname(__FILE__) . '/archive.php');
require_once(dirname(__FILE__) . '/target.php');

class Backup
{
	private $config;
	private $system;
	private $site;
	private $schedule;
	private $backupNeeded = false;
	private $fileBackup = false;
	private $dbBackup = false;
	private $backupFreq = 0;
	private $lastBackup;
	private $logfile;
	private $dbBackupFilename;
	
	public function __construct() 
	{
		$this->config = new SiteMaintenanceConfig();
		$this->system = new System();
		$this->logfile = $this->config->logfile;
		$this->dbBackupFilename = 'full-db-dump.sql';
	}
	
	public function getSchedules($id)
	{
		$config = $this->config;
		$dbname = $config->dbName;
		$username = $config->dbUser;
		$password = $config->dbPassword;
			
		$conn = new \PDO("mysql:host=localhost;dbname=$dbname", $username, $password);
		$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

		$stmt = $conn->prepare('SELECT id, backupType, backupPath, backupFreq, 
					last_backup AS lastBackup, archiveSize, databaseType, 
					siteDbUsername, siteDbName, siteDbEncryptedPasswordPath,
					DATEDIFF( NOW( ) , last_backup ) AS daysSinceBackup
				FROM `siteSchedule`
				WHERE sourceID = :sourceID
			');
		
		$stmt->bindParam(':sourceID', $id, \PDO::PARAM_INT);
		$stmt->execute();
		
		$rows = $stmt->rowCount();
		
		$msg = "SiteID: " . $id . " -- ";
		$msg .= $rows . " Backup Schedule(s) found. ";
		$this->system->writeLog($msg , $this->logfile);
		
		$schedules = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		if (empty($schedules)) {
			return false;
		}
		
		return $schedules;
	}
	
	public function setSchedule(array $site, array $schedule)
	{
		$this->site = $site;
		$this->schedule = $schedule;
		$this->setBackupNeeded();
		
		if ($this->backupNeeded) {
			$this->setBackupTypes();
		}
	}
	
	private function setBackupNeeded()
	{
		$backupFreq = $this->schedule['backupFreq'];
		$lastBackup = $this->schedule['lastBackup'];
		$daysSinceBackup = $this->schedule['daysSinceBackup'];
		$backupType = $this->schedule['backupType'];
		$systemName = $this->site['systemName'];
		
		if (empty ($lastBackup) ) {
			$this->backupNeeded = true;
			$msg = $systemName . ' -- No backups scheduled.';
			$this->system->writeLog($msg, $this->logfile);
		}
		elseif ($daysSinceBackup >= $backupFreq) {
			$msg = $systemName . ' -- ' . $backupType . ' Scheduled.';
			$this->backupNeeded = true;
		}
		else {
			$msg = $systemName . ' -- No backups scheduled.';
			$this->system->writeLog($msg, $this->logfile);
		}
		
		if ($backupFreq == 0) {
			$this->backupNeeded = false;
			$msg = $systemName . ' -- Schedule Disabled';
			$this->system->writeLog($msg, $this->logfile);
		}
		
	}
	
	private function setBackupTypes()
	{
		$backupType = $this->schedule['backupType'];
		if ($backupType == 'BOTH' || $backupType == 'FILE') {
			$this->fileBackup = true;
			$this->system->writeLog('File Backup set to true.', $this->logfile);
		}
		if ($backupType == 'BOTH' || $backupType == 'DB') {
			$this->dbBackup = true;
			$this->system->writeLog('Database Backup set to true.', $this->logfile);
		}
	}
	
	
	public function checkBackupNeeded()
	{
		return $this->backupNeeded;
	}
	
	public function backup()
	{
		if (!$this->backupNeeded) {
			return false;
		}
		
		if ($this->fileBackup) {
			$this->backupFiles();
		}
		else {
			$this->system->writeLog('File backup not set.' , $this->logfile);
		}
		
		if ($this->dbBackup) {
			$this->backupDatabase();
			if ( $this->site['sourceLocal'] == 0  ) {
				$this->transferRemoteDBBackup();
			}
		}
		else {
			$this->system->writeLog('Database backup not set.' , $this->logfile);
		}
		
		$arc = new Archive();
		$archiveFilepath = $arc->createArchive($this->site, $this->schedule);
		
		$this->system->removeDir('tmp/');
		
		if ($this->testBackupArchive($archiveFilepath, $this->site, $this->schedule) ) {
			$msg = 'Backup process completed successfully for ' . $this->site['systemName'];
			$this->system->writeLog($msg , $this->logfile);
			return true;
		}
		else {
			$msg = 'There was at least one issue found when testing the backup for ' . $this->site['systemName'];
			$this->system->writeLog($msg , $this->logfile, 'WARNING');
			return false;
		}
		
		
	}
			
	private function backupFiles()
	{
		$cmd = $this->getFileSyncCommand();
		$output = array();
		$status = null;

		exec($cmd, $output, $status);
		
		if ($status == 0) {
			$msg = "Site files transferred successfully.";
			$this->system->writeLog($msg , $this->logfile);
			return true;
		}
		else {
				$msg = 'Error processing files for SiteID:  ' . $this->site['id'] ;
				$msg .= '  Status Code:  ' . $status;
				$this->system->writeLog($msg , $this->logfile, 'ERROR');
				return false;
			}
	}
	
	private function backupDatabase()
	{
		$databaseType = $this->schedule['databaseType'];

		if ($databaseType == 'MySQL') {
			$command = $this->getMySQLBackupCommand();
		}
		else {
			$msg = 'Database selection not valid!  Type stored in database:  ' . $databaseType;
			$this->system->writeLog($msg , $this->logfile);
		}
		
		$output = array();
		$status = null;
		exec ($command, $output, $status);
		
		if ($status > 0 ) {
				$msg = 'Error processing database for SiteID:  ' . $this->site['id'] ;
				$msg .= '  Status Code:  ' . $status;
				$this->system->writeLog($msg , $this->logfile, 'ERROR');
		}
	}
	
	private function getFileSyncCommand()
	{
		$config = $this->config;
		$tmpPathLocal = 'tmp/';
		
		$username = $this->site['username'];
		$remoteHost = $this->site['remoteHost'];
		$backupPath = $this->schedule['backupPath'];		
		$sourceLocal = $this->site['sourceLocal'];
		
		if ($sourceLocal == 0) {
			$ssh = ' "' . $config->pathToSSH . ' -i ' . $config->keyPrivateSSH . '" ';
			$cmd = 'rsync -az -e ' .$ssh . ' ' . $username . '@' . $remoteHost . ':' . $backupPath . ' ' . $tmpPathLocal;
		}
		elseif ($sourceLocal == 1) {
			$cmd = 'rsync -avz ' . $backupPath . ' ' . $tmpPathLocal;
		}
		
		return $cmd;
	}
	
	private function getMySQLBackupCommand()
	{
		$system = new System();
		$system->createDir('tmp/');
		$system->createDir('tmp/sql/');
		
		$config = $this->config;
		$sourceLocal = $this->site['sourceLocal'];
		$sshUser = $this->site['username'];
		$sshHost = $this->site['remoteHost'];
		$user = $this->schedule['siteDbUsername'];
		
		$pw = $this->decryptSiteDbPassword();
		$db = $this->schedule['siteDbName'];
		
		if ($sourceLocal == 0) {
				$filepath = $this->dbBackupFilename;
				$command = "ssh -i $config->keyPrivateSSH $sshUser@$sshHost ";
				$command .= "'mysqldump --skip-comments --skip-extended-insert ";
				$command .= " -u$user -p$pw $db > " . escapeshellarg($filepath) . "'";
			}
		elseif ($sourceLocal == 1) {		
				$filepath = "tmp/sql/" . $this->dbBackupFilename;
				$command = "mysqldump --skip-comments --skip-extended-insert ";
				$command .= " -u$user -p$pw $db > " . escapeshellarg($filepath) ;
			}
		
		return $command;
	}

	private function transferRemoteDBBackup()
	{
		$tmpPathLocal = 'tmp/sql';
		
		$sshUser = $this->site['username'];
		$sshHost = $this->site['remoteHost'];
		
		$output = array();
		$status = null;
		
		
		$ssh = ' "' . $this->config->pathToSSH . ' -i ' . $this->config->keyPrivateSSH . '" ';
		$cmd = 'rsync --remove-source-files -avz -e ' .$ssh . ' ';
		$cmd .= $sshUser . '@' . $sshHost . ':' . escapeshellarg($this->dbBackupFilename) . ' ' . escapeshellarg($tmpPathLocal);
		exec($cmd, $output, $status);
		
		return $status;
	}
	
	private function decryptSiteDbPassword()
	{
		$decrypt = new decrypt();
		$encryptedPwPath = $this->schedule['siteDbEncryptedPasswordPath'];
		$decrypted = $decrypt->execute($encryptedPwPath);
		return $decrypted;
	}
	
	private function testBackupArchive($filepath, array $site, array $schedule)
	{
		$t = new Target();
		$a = new Archive();
		
		$targetID = $t->getAvailableTarget();
		if (!$targetID) {
			$msg = 'There was no available target.  File ' . $filepath . ' NOT tested.';
			$this->system->writeLog($msg, $this->logfile, 'WARNING');
			return false;
		}
		$set = $t->setTarget( $targetID );
		
		if (! $set ) {
			$msg = 'There was a problem setting the target.  File ' . $filepath . ' NOT tested.';
			$this->system->writeLog($msg, $this->logfile, 'WARNING');
			return false;
		}
		
		$t->cleanTarget();
		$targetPath = $t->getTargetPath();
		
		if (! $a->extractArchive($filepath, $targetPath) ) {
			$msg = 'There was a problem extracting the target archive file.  File: ' . $filepath . ' NOT tested.';
			$this->system->writeLog($msg, $this->logfile, 'WARNING');
			return false;
		}
		
		$dbLoadStatus = $t->loadDb($targetPath . '/sql/full-db-install.sql');
		if ($dbLoadStatus > 0 ) {
			$msg = 'There was a problem loading the database for archive: ' . $filepath  ;
			$this->system->writeLog($msg, $this->logfile, 'WARNING');
			return false;
		}
		
		$t->cleanTarget();
		
		// Unlock target
		
	}
	
}