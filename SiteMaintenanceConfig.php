<?php
	namespace SiteMaintenance;

class SiteMaintenanceConfig
{
	public $keyPublicSSH = '/path/to/.ssh/id_rsa.pub';
	public $keyPrivateSSH = '/path/to/.ssh/id_rsa';
	public $keyPublicDecrypt = '/path/to/.ssh/public_key_name.pem';
	public $keyPrivateDecrypt = '/path/to/.ssh/private_key_name.pem';
	public $backupPath = '/path/to/backup/storage';  // This is typically the "recent" path
	public $backupPath2 = '/path/to/weekly/backups';
	public $backupPath3 = '/path/to/monthly/backups';
	public $dbName = 'db_name';
	public $dbUser = 'db_user';
	public $dbPassword = 'db_password';
	public $pathToSSH = '/path/to/ssh';
	public $logfile = 'sm.log';
}