<?php
namespace SiteMaintenance;

require_once(dirname(__FILE__) . '/../SiteMaintenanceConfig.php');

class templateManager
{
	private $config;
	private $ordersPending;
	private $orders;
	
	public function __construct()
	{
		$config = new SiteMaintenanceConfig();
		$this->config = $config;
		$this->ordersPending = false;
	}
	
	public function checkForOrderToBuildTemplate()
	{
		$config = $this->config;
		$dbname = $config->dbName;
		$username = $config->dbUser;
		$password = $config->dbPassword;
		
		$conn = new \PDO("mysql:host=localhost;dbname=$dbname", $username, $password);
		$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		
		$stmt = $conn->prepare('SELECT t.id as templateID, t.templateName, 
					t.templateSourceID, t.templateUserID, s.systemName
				FROM template t
				INNER JOIN source s ON t.templateSourceID = s.id
				WHERE t.templateCreated IS NULL
				LIMIT 1
			');
		$stmt->execute();
		$orders = $stmt->fetchAll();
		
		if ( count($orders) > 0 ) {
			$this->orders = $orders;
			$this->ordersPending = true;
			return true;
		}
		
		return false;
	}
	
	public function buildPendingTemplate()
	{
		if (! $this->ordersPending) {
			return false;
		}
		
		foreach ($this->orders as $order) {
			$status = $this->createTemplate($order);
			
			if ($status == 0) {
				$this->updateDatabase($order);
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	
	private function createTemplate(array $order)
	{
		$config = $this->config;
		$templatePath = $config->templatePath;
		$backupPath = $config->backupPath;
		
		$templateName = $order['templateName'];
		$systemName = $order['systemName'];
		$archiveName = $systemName . '_archive.zip';
		
		$cmd = 'cp ' . $backupPath . '/' . $archiveName . ' ' . $templatePath . '/' . $templateName . '.zip' ;
		$output = array();
		$status = null;
		exec($cmd, $output, $status);
		return $status;
	}
	
	private function updateDatabase(array $order)
	{
		$templateID = $order['templateID'];
		$config = $this->config;
		$dbname = $config->dbName;
		$username = $config->dbUser;
		$password = $config->dbPassword;
		
		$conn = new \PDO("mysql:host=localhost;dbname=$dbname", $username, $password);
		$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		
		$stmt = $conn->prepare('UPDATE template SET templateCreated = NOW() 
				WHERE id = :templateID ');
		$stmt->execute(array('templateID' => (int) $templateID));
	}
}