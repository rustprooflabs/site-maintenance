<?php
namespace SiteMaintenance;

require_once(dirname(__FILE__) . '/../SiteMaintenanceConfig.php');

class transferArchive
{
	private $orders;
	private $dbName;
	private $dbUsername;
	private $dbPassword;
	private $pathToRecentBackups;
	private $pathToSSH;
	private $keyPrivateSSH;
	
	public function __construct()
	{
		$config = new SiteMaintenanceConfig();
		$this->dbName = $config->dbName;
		$this->dbUsername = $config->dbUser;
		$this->dbPassword = $config->dbPassword;
		$this->pathToRecentBackups = $config->backupPath;
		$this->pathToSSH = $config->pathToSSH;
		$this->keyPrivateSSH = $config->keyPrivateSSH;
	}
	
	
	public function processOrders()
	{
		$processed = 0;
		if ($this->getTransferOrders() ) {
			$orders = $this->orders;
			foreach ($orders as $order ) {
				if ($this->executeOrder($order) ) {
					$transferID = $order['transferID'];
					$this->completeTransfer((int) $transferID);
					$processed = $processed + 1;
				}
			}
		}
		
		return $processed;
	}
	
	public function getTransferOrders()
	{
		$dbname = $this->dbName;
		$username = $this->dbUsername;
		$password = $this->dbPassword;
		
		$status = 'Pending';
		
		$conn = new \PDO("mysql:host=localhost;dbname=$dbname", $username, $password);
		$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		
		$stmt = $conn->prepare('SELECT t.id as transferID, t.status, t.userID, t.createdDate, 
					s.systemName AS sourceName, s.remoteHost, s.username, s.transferDropboxPath
				FROM transfer t 
				INNER JOIN source s ON t.sourceID = s.id
				WHERE t.status = :status
			');
		$stmt->execute(array('status' => $status));
		
		$orders = $stmt->fetchAll();
		
		if ( count($orders) > 0 ) {
			$this->orders = $orders;
			return true;
		}
		
		return false;
	}
	
	
	public function executeOrder( array $order = array() )
	{
		$pathToLocalFile = $this->pathToRecentBackups . '/' . $order['sourceName'] . '_archive.zip';
		$remoteUser = $order['username'];
		$remoteHost = $order['remoteHost'];
		$remotePath = $order['transferDropboxPath'];
		
		$status = $this->sendFileRsync($pathToLocalFile, $remoteHost, $remoteUser, $remotePath);
		
		if ($status == 0) {
			return true;
		}
		else {
			return false;
		}
		
	}
	
	
	public function sendFileRsync($pathToLocalFile, $remoteHost, $remoteUser, $remotePath)
	{
		$pathToSSH = $this->pathToSSH;
		$keyPrivateSSH = $this->keyPrivateSSH;
		
		$output = array();
		$status = null;
		
		$ssh = ' "' . $pathToSSH . ' -i ' . $keyPrivateSSH . '" ';
		$cmd = 'rsync -avz -e ' .$ssh . ' ' . $pathToLocalFile . ' ' . $remoteUser . '@' . $remoteHost . ':' . $remotePath;
		exec($cmd, $output, $status);
		
		return $status;
		
	}
	
	private function completeTransfer($transferID)
	{
		$dbname = $this->dbName;
		$username = $this->dbUsername;
		$password = $this->dbPassword;
		
		$conn = new \PDO("mysql:host=localhost;dbname=$dbname", $username, $password);
		$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

		$status = 'Pending';
		$newStatus = 'Completed';
		
		$stmt = $conn->prepare('UPDATE transfer SET status = :newStatus, statusDate = NOW()
			WHERE id = :transferID AND status = :status');
		$stmt->execute(array('newStatus' => $newStatus, 'transferID' => $transferID, 'status' => $status));
	}
}