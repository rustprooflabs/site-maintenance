<?php
namespace SiteMaintenance;

require_once(dirname(__FILE__) . '/app/RestoreArchive.php');

$siteID = (int) $argv[1];
$ra = new RestoreArchive();
$ra->restoreArchive($siteID);

?>
